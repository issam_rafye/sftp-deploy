#!/usr/bin/env bash
#
# Test SFTP
#

set -e

# Can be run multiple times and takes an arg to add to the file contents to differentiate runs
fileSetup() {
  # parameters
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}

  # local variables
  RANDOM_NUMBER=$RANDOM
  [[ "${RANDOM_NUMBER}" -ne "" ]]
  BASE_LOCAL_DIR="tmp"
  LOCAL_DIR="$BASE_LOCAL_DIR/sftp/"
  FILE_CONTENTS="Pipelines is awesome ${RANDOM_NUMBER}! $1"

  # clean up & create paths
  rm -rf $BASE_LOCAL_DIR

  mkdir -p $LOCAL_DIR/subdir
  FILE1=deployment-${RANDOM_NUMBER}-1.txt
  FILE2=deployment-${RANDOM_NUMBER}-2.txt
  FILE3=subdir/deployment-${RANDOM_NUMBER}-3.txt
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE1
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE2
  echo $FILE_CONTENTS > $LOCAL_DIR/$FILE3

  TARGET_HOST=52.65.134.106
  SSH_CONFIG_DIR=/opt/atlassian/pipelines/agent/ssh
}

# Simulate a known hosts failure by removing known_hosts entries.
badKnownHostsSetup() {
  BAD_SSH_CONFIG_DIR=/opt/atlassian/pipelines/agent/build/bad_ssh
  mkdir ${BAD_SSH_CONFIG_DIR}
  cp -rL ${SSH_CONFIG_DIR}/* ${BAD_SSH_CONFIG_DIR}
  echo "" > ${BAD_SSH_CONFIG_DIR}/known_hosts
}

teardown() {
  # clean up
    rm -rf $BASE_LOCAL_DIR
}

run() {
  echo "$@"
  set +e
  output=$("$@" 2>&1)
  status=$?
  set -e
  echo "${output}"
}

shouldWork() {
  # execute tests
  run docker run \
      -a STDOUT -a STDERR \
      -e USER="ec2-user" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e EXTRA_ARGS="-oPort=22" \
      -e DEBUG="true" \
      -v $(pwd):$(pwd) \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -w $(pwd) \
      $IMAGE_NAME

  [[ "${status}" == "0" ]]
}

# verify

deployment1() {
  run curl -s http://${TARGET_HOST}/sftp/$FILE1
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]
}

deployment2() {
  run curl -s http://${TARGET_HOST}/sftp/$FILE2
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]
}
deployment3() {
  run curl -s http://${TARGET_HOST}/sftp/$FILE3
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${FILE_CONTENTS}" ]]
}
shouldFailBadUser() {
  # execute tests
  run docker run \
      -a STDOUT -a STDERR \
      -e USER="ec2-user-will-not-work" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e EXTRA_ARGS="-oPort=22" \
      -e DEBUG="true" \
      -v $(pwd):$(pwd) \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -w $(pwd) \
      $IMAGE_NAME

  [[ "${status}" != "0" ]]
}

shouldFailBadKnownHosts() {
  # execute tests
  run docker run \
      -a STDOUT -a STDERR \
      -e USER="ec2-user" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e DEBUG="true" \
      -v $(pwd):$(pwd) \
      -v ${BAD_SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -w $(pwd) \
      $IMAGE_NAME

  [[ "${status}" != "0" ]]
}

shouldPassSSHKey() {
  # execute tests
  run docker run \
      -a STDOUT -a STDERR \
      -e USER="ec2-user" \
      -e SERVER="${TARGET_HOST}" \
      -e REMOTE_PATH="/var/www/html" \
      -e LOCAL_PATH="$LOCAL_DIR" \
      -e SSH_KEY="${SSH_KEY}" \
      -e EXTRA_ARGS="-oPort=22" \
      -e DEBUG="true" \
      -v $(pwd):$(pwd) \
      -v ${SSH_CONFIG_DIR}:${SSH_CONFIG_DIR} \
      -w $(pwd) \
      $IMAGE_NAME

  [[ "${status}" == "0" ]]
}

blue="\\e[36m"
reset="\\e[0m"
runtest(){
    echo
    echo -e "${blue}Running test '"$1"'${reset}"
    echo
    $1
}


fileSetup run1
runtest shouldWork
runtest deployment1
runtest deployment2
runtest deployment3
runtest shouldFailBadUser
badKnownHostsSetup
runtest shouldFailBadKnownHosts
fileSetup run2
runtest shouldPassSSHKey
runtest deployment1
runtest deployment2
runtest deployment3
